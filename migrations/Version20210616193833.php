<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210616193833 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE bugs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE categories_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE contact_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE logs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE mon_sac_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE pays_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE reset_password_request_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE users_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE voyages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bugs (id INT NOT NULL, user_id INT DEFAULT NULL, dateTime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, credicite INT NOT NULL, url VARCHAR(100) NOT NULL, commentaires VARCHAR(100) NOT NULL, etat INT NOT NULL, dateTimeRetour TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, retour VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1E197C9A76ED395 ON bugs (user_id)');
        $this->addSql('CREATE TABLE categories (id INT NOT NULL, parent_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name VARCHAR(100) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3AF34668727ACA70 ON categories (parent_id)');
        $this->addSql('CREATE TABLE contact (id INT NOT NULL, user_id INT DEFAULT NULL, dateTime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, mail VARCHAR(100) NOT NULL, ip VARCHAR(50) NOT NULL, nom VARCHAR(50) NOT NULL, telephone VARCHAR(13) NOT NULL, texte VARCHAR(255) NOT NULL, valid BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4C62E638A76ED395 ON contact (user_id)');
        $this->addSql('CREATE TABLE logs (id INT NOT NULL, user_id INT DEFAULT NULL, dateTime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, divers VARCHAR(255) NOT NULL, etat VARCHAR(50) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F08FC65CA76ED395 ON logs (user_id)');
        $this->addSql('CREATE TABLE mon_sac (id INT NOT NULL, users_id INT NOT NULL, categories_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, active BOOLEAN NOT NULL, url VARCHAR(700) NOT NULL, budgets_prevus INT NOT NULL, budgets_paye INT NOT NULL, etat INT NOT NULL, date_achat DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C529250167B3B43D ON mon_sac (users_id)');
        $this->addSql('CREATE INDEX IDX_C5292501A21214B7 ON mon_sac (categories_id)');
        $this->addSql('CREATE TABLE pays (id INT NOT NULL, name VARCHAR(255) NOT NULL, nom_alpha VARCHAR(255) NOT NULL, code VARCHAR(255) NOT NULL, article VARCHAR(255) NOT NULL, nom_long VARCHAR(255) NOT NULL, capitale VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE reset_password_request (id INT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7CE748AA76ED395 ON reset_password_request (user_id)');
        $this->addSql('COMMENT ON COLUMN reset_password_request.requested_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN reset_password_request.expires_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE users (id INT NOT NULL, pays_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_verified BOOLEAN NOT NULL, name VARCHAR(100) NOT NULL, firstname VARCHAR(100) NOT NULL, pseudo VARCHAR(20) NOT NULL, date_naissance TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, sexe BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE INDEX IDX_1483A5E9A6E44244 ON users (pays_id)');
        $this->addSql('CREATE TABLE voyages (id INT NOT NULL, users_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, titre VARCHAR(255) NOT NULL, date_time_debut TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, date_time_fin TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, participant_nb_adultes INT NOT NULL, participant_nb_enfants INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_30F7F967B3B43D ON voyages (users_id)');
        $this->addSql('CREATE TABLE voyages_pays (voyages_id INT NOT NULL, pays_id INT NOT NULL, PRIMARY KEY(voyages_id, pays_id))');
        $this->addSql('CREATE INDEX IDX_1344B517A170CAB9 ON voyages_pays (voyages_id)');
        $this->addSql('CREATE INDEX IDX_1344B517A6E44244 ON voyages_pays (pays_id)');
        $this->addSql('ALTER TABLE bugs ADD CONSTRAINT FK_1E197C9A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE categories ADD CONSTRAINT FK_3AF34668727ACA70 FOREIGN KEY (parent_id) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE contact ADD CONSTRAINT FK_4C62E638A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE logs ADD CONSTRAINT FK_F08FC65CA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mon_sac ADD CONSTRAINT FK_C529250167B3B43D FOREIGN KEY (users_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mon_sac ADD CONSTRAINT FK_C5292501A21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9A6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voyages ADD CONSTRAINT FK_30F7F967B3B43D FOREIGN KEY (users_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voyages_pays ADD CONSTRAINT FK_1344B517A170CAB9 FOREIGN KEY (voyages_id) REFERENCES voyages (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE voyages_pays ADD CONSTRAINT FK_1344B517A6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE categories DROP CONSTRAINT FK_3AF34668727ACA70');
        $this->addSql('ALTER TABLE mon_sac DROP CONSTRAINT FK_C5292501A21214B7');
        $this->addSql('ALTER TABLE users DROP CONSTRAINT FK_1483A5E9A6E44244');
        $this->addSql('ALTER TABLE voyages_pays DROP CONSTRAINT FK_1344B517A6E44244');
        $this->addSql('ALTER TABLE bugs DROP CONSTRAINT FK_1E197C9A76ED395');
        $this->addSql('ALTER TABLE contact DROP CONSTRAINT FK_4C62E638A76ED395');
        $this->addSql('ALTER TABLE logs DROP CONSTRAINT FK_F08FC65CA76ED395');
        $this->addSql('ALTER TABLE mon_sac DROP CONSTRAINT FK_C529250167B3B43D');
        $this->addSql('ALTER TABLE reset_password_request DROP CONSTRAINT FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE voyages DROP CONSTRAINT FK_30F7F967B3B43D');
        $this->addSql('ALTER TABLE voyages_pays DROP CONSTRAINT FK_1344B517A170CAB9');
        $this->addSql('DROP SEQUENCE bugs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE categories_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE contact_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE logs_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE mon_sac_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE pays_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE reset_password_request_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE users_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE voyages_id_seq CASCADE');
        $this->addSql('DROP TABLE bugs');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE logs');
        $this->addSql('DROP TABLE mon_sac');
        $this->addSql('DROP TABLE pays');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE voyages');
        $this->addSql('DROP TABLE voyages_pays');
    }
}
