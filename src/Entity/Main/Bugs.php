<?php
namespace App\Entity\Main;

use App\Entity\Users;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=BugsRepository::class)
 * @ApiResource
 */
class Bugs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTime", type="datetime")
     */
    private $dateTime;

    /**
     * @var integer
     * @ORM\Column(name="credicite", type="integer", length=1)
     */
    private $credicite;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaires", type="string", length=100)
     */
    private $commentaires;

    /**
     * @var integer
     * @ORM\Column(name="etat", type="integer", length=1)
     */
    private $etat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTimeRetour", type="datetime")
     */
    private $dateTimeRetour;

    /**
     * @var string
     *
     * @ORM\Column(name="retour", type="string", length=100)
     */
    private $retour;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="bugs")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;


    public function __construct()
    {
        $this->dateTime = new \DateTime();
        $this->retour = "_";
        $this->dateTimeRetour = new \DateTime();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     *
     * @return Bugs
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Set credicite
     *
     * @param integer $credicite
     *
     * @return Bugs
     */
    public function setCredicite($credicite)
    {
        $this->credicite = $credicite;

        return $this;
    }

    /**
     * Get credicite
     *
     * @return integer
     */
    public function getCredicite()
    {
        return $this->credicite;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Bugs
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set commentaires
     *
     * @param string $commentaires
     *
     * @return Bugs
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    /**
     * Get commentaires
     *
     * @return string
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * Set etat
     *
     * @param integer $etat
     *
     * @return Bugs
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * Set retour
     *
     * @param string $retour
     *
     * @return Bugs
     */
    public function setRetour($retour)
    {
        $this->retour = $retour;

        return $this;
    }

    /**
     * Get retour
     *
     * @return string
     */
    public function getRetour()
    {
        return $this->retour;
    }

    /**
     * Set dateTimeRetour
     *
     * @param \DateTime $dateTimeRetour
     *
     * @return Bugs
     */
    public function setDateTimeRetour($dateTimeRetour)
    {
        $this->dateTimeRetour = $dateTimeRetour;

        return $this;
    }

    /**
     * Get dateTimeRetour
     *
     * @return \DateTime
     */
    public function getDateTimeRetour()
    {
        return $this->dateTimeRetour;
    }


    /**
     * Set user
     *
     * @param \App\Entity\Users;
     * @return Bugs
     */
    public function getUser(): ?Users
    {
        return $this->user;
    }

    /**
     * Get user
     *
     * @return \App\Entity\Users;
     */
    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

}