<?php
namespace App\Entity\Main;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Users;
use ApiPlatform\Core\Annotation\ApiResource;


/**
 * @ORM\Entity(repositoryClass=ContactRepository::class)
 * @ApiResource
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTime", type="datetime")
     */
    private $dateTime;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="voyages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="mail", type="string", length=100)
     * @Assert\NotBlank(message="Merci d'indiquer votre email.")
     * @Assert\Email(
     *     message = "Votre email '{{ value }}' n'est pas valide.",
     * )
     */
    private $mail;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=50)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50)
     * @Assert\NotBlank(message="Merci d'indiquer votre nom.")
     *
     */
    private $nom;

    /**
     * @var string
     * @Assert\NotBlank(message="Merci d'indiquer votre numéro de téléphone.")
     * @Assert\Length(
     *      min = 10,
     *      max = 13,
     *      minMessage = "Votre numéro doit comporter {{ limit }} chiffres au minimum",
     *      maxMessage = "Votre numéro doit comporter {{ limit }} chiffres au maximum",
     * )
     * @ORM\Column(name="telephone", type="string", length=13)
     */
    private $telephone;

    /**
     * @var string
     * @Assert\NotBlank(message="Merci d'indiquer un commentaire.")
     * @Assert\Length(
     *      min = 10,
     *      max = 255,
     *      minMessage = "Votre commentaire doit comporter {{ limit }} caractères au minimum",
     *      maxMessage = "Votre commentaire doit comporter {{ limit }} caractères au maximum",
     * )
     * @ORM\Column(name="texte", type="string", length=255)
     */
    private $texte;

    /**
     * @var boolean
     *
     * @ORM\Column(name="valid", type="boolean")
     */
    private $valid;

    public function __construct()
    {
        $this->dateTime = new \DateTime();
        $this->valid = 0;
        $this->utilisateurs = null;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     * @return contact
     */
    public function setDateTime($dateTime)
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime 
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return contact
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return contact
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return contact
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return contact
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set texte
     *
     * @param string $texte
     * @return contact
     */
    public function setTexte($texte)
    {
        $this->texte = $texte;

        return $this;
    }

    /**
     * Get texte
     *
     * @return string 
     */
    public function getTexte()
    {
        return $this->texte;
    }

    /**
     * Set valid
     *
     * @param boolean $valid
     * @return contact
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return boolean 
     */
    public function getValid()
    {
        return $this->valid;
    }


    /**
     * Set user
     *
     * @param \App\Entity\Users;
     * @return Contact
     */
    public function getUser(): ?Users
    {
        return $this->user;
    }

    /**
     * Get user
     *
     * @return \App\Entity\Users;
     */
    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }
}
