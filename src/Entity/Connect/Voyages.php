<?php

namespace App\Entity\Connect;

use App\Repository\VoyagesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Entity\Users;
use App\Entity\Main\Pays;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=VoyagesRepository::class)
 */
class Voyages
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="voyages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titre;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTime_debut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTime_fin;

    /**
     * @ORM\Column(type="integer")
     */
    private $participantNbAdultes;

    /**
     * @ORM\Column(type="integer")
     */
    private $participantNbEnfants;

    /**
     * @ORM\ManyToMany(targetEntity=Pays::class, inversedBy="voyages")
     */
    private $pays;

    public function __construct()
    {
        $this->pays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDateTimeDebut(): ?\DateTimeInterface
    {
        return $this->dateTime_debut;
    }

    public function setDateTimeDebut(\DateTimeInterface $dateTime_debut): self
    {
        $this->dateTime_debut = $dateTime_debut;

        return $this;
    }

    public function getDateTimeFin(): ?\DateTimeInterface
    {
        return $this->dateTime_fin;
    }

    public function setDateTimeFin(\DateTimeInterface $dateTime_fin): self
    {
        $this->dateTime_fin = $dateTime_fin;

        return $this;
    }

    public function getParticipantNbAdultes(): ?int
    {
        return $this->participantNbAdultes;
    }

    public function setParticipantNbAdultes(int $participantNbAdultes): self
    {
        $this->participantNbAdultes = $participantNbAdultes;

        return $this;
    }

    public function getParticipantNbEnfants(): ?int
    {
        return $this->participantNbEnfants;
    }

    public function setParticipantNbEnfants(int $participantNbEnfants): self
    {
        $this->participantNbEnfants = $participantNbEnfants;

        return $this;
    }

    /**
     * @return Collection|Pays[]
     */
    public function getPays(): Collection
    {
        return $this->pays;
    }

    public function addPay(Pays $pay): self
    {
        if (!$this->pays->contains($pay)) {
            $this->pays[] = $pay;
        }

        return $this;
    }

    public function removePay(Pays $pay): self
    {
        $this->pays->removeElement($pay);

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
