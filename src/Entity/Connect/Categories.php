<?php

namespace App\Entity\Connect;

use App\Repository\CategoriesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Connect\MonSac;

/**
 * @ORM\Entity(repositoryClass=CategoriesRepository::class)
 * @ApiResource
 */
class Categories
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(type="string", length=255)
     */
    private $Slug;

    /**
     * @ORM\ManyToOne(targetEntity=Categories::class, inversedBy="categories")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity=Categories::class, mappedBy="parent")
     */
    private $categories;

    /**
     * @ORM\OneToMany(targetEntity=MonSac::class, mappedBy="categories")
     */
    private $MonSac;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->MonSac = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->Slug;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(self $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->setParent($this);
        }

        return $this;
    }

    public function removeCategory(self $category): self
    {
        if ($this->categories->removeElement($category)) {
            // set the owning side to null (unless already changed)
            if ($category->getParent() === $this) {
                $category->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|MonSac[]
     */
    public function getMonSac(): Collection
    {
        return $this->MonSac;
    }

    public function addSacado(MonSac $sacado): self
    {
        if (!$this->MonSac->contains($sacado)) {
            $this->MonSac[] = $sacado;
            $sacado->setCategories($this);
        }

        return $this;
    }

    public function removeSacado(MonSac $sacado): self
    {
        if ($this->MonSac->removeElement($sacado)) {
            // set the owning side to null (unless already changed)
            if ($sacado->getCategories() === $this) {
                $sacado->setCategories(null);
            }
        }

        return $this;
    }
    
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function setSlug(string $Slug): self
    {
        $this->Slug = $Slug;

        return $this;
    }

    public function addMonSac(MonSac $monSac): self
    {
        if (!$this->MonSac->contains($monSac)) {
            $this->MonSac[] = $monSac;
            $monSac->setCategories($this);
        }

        return $this;
    }

    public function removeMonSac(MonSac $monSac): self
    {
        if ($this->MonSac->removeElement($monSac)) {
            // set the owning side to null (unless already changed)
            if ($monSac->getCategories() === $this) {
                $monSac->setCategories(null);
            }
        }

        return $this;
    }
}
