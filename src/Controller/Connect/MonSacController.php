<?php

namespace App\Controller\Connect;

use App\Entity\Connect\MonSac;
use App\Repository\MonSacRepository;
use App\Form\EditProfileType;
use App\Form\MonSacType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/connect/monsac", name="connect_monsac_")
 * @package App\Controller
 */
class MonSacController extends AbstractController
{

    public $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

    /**
    * 
    * @Route("/", name="home")
    */
   public function index(MonsacRepository $monsacRepo)
   {
       $sens = "DESC";
       $search = "";
       $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];


       $Donnees = $monsacRepo->findOneBy(['users' => $this->getUser()]);

       return $this->render('connect/monsac/index.html.twig', [
           'Donnees' => $Donnees,
           'sens1' => $sens,
           'find' => $search,
           'listeEtat' => $listeEtat,
           'controller_name' => 'connect_monsac_',
       ]);
   }


    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(MonsacRepository $monsacRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

        $repository = $this->getDoctrine()->getRepository(Monsac::class);
        $listDonnees = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/monsac/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search,
            'listeEtat' => $listeEtat,
            'controller_name' => 'connect_monsac_',
        ]);
    }





    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutMonSac(Request $request): Response
    {
        $MonSac = new MonSac;

        $form =$this->createForm(MonSacType::class, $MonSac);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $MonSac->setUsers($this->getUser());
            $MonSac->setActive(false);
            
            $em->persist($MonSac);
            $em->flush();

            return $this->redirectToRoute('admin_home');
        }

        return $this->render('connect/MonSac/ajout.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'connect_monsac_',
        ]);
    }

}
