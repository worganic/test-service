<?php

namespace App\Controller\Connect;

use App\Entity\Connect\Categories;
use App\Entity\Connect\MonSac;
use App\Form\CategoriesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/connect", name="connect_")
 * @package App\Controller
 */
class ConnectController extends AbstractController
{
    /**
     * 
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('connect/index.html.twig', [
            'controller_name' => 'connect_home',
        ]);
    }

}