<?php

namespace App\Controller\Connect;

use App\Entity\Connect\Categories;
use App\Entity\Connect\MonSac;
use App\Form\CategoriesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/connect/contact", name="connect_contact_")
 * @package App\Controller
 */
class ContactController extends AbstractController
{
    /**
     * 
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('connect/contact/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

}