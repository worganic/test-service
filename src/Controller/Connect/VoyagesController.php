<?php

namespace App\Controller\Connect;

use App\Entity\Connect\Voyages;
use App\Repository\VoyagesRepository;
use App\Form\EditProfileType;
use App\Form\VoyagesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/connect/voyages", name="connect_voyages_")
 * @package App\Controller
 */
class VoyagesController extends AbstractController
{

    public $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

    /**
    * 
    * @Route("/", name="home")
    */
   public function index(VoyagesRepository $voyagesRepo)
   {
       $sens = "DESC";
       $search = "";

       $Donnees = $voyagesRepo->findOneBy(['users' => $this->getUser()]);

       return $this->render('connect/voyages/index.html.twig', [
           'Donnees' => $Donnees,
           'sens1' => $sens,
           'find' => $search,
           'controller_name' => 'connect_voyages_',
       ]);
   }


    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(VoyagesRepository $voyagesRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

        $repository = $this->getDoctrine()->getRepository(Voyages::class);
        $listDonnees = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/voyages/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search,
            'controller_name' => 'connect_voyages_',
        ]);
    }





    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutVoyages(Request $request): Response
    {
        $voyages = new Voyages;

        $form =$this->createForm(VoyagesType::class, $voyages);
        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();

            $voyages->setUsers($this->getUser());
         //   $voyages->setActive(false);
            
            $em->persist($voyages);
            $em->flush();

            return $this->redirectToRoute('connect_voyages_');
        }

        return $this->render('connect/voyages/ajout.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'connect_voyages_',
        ]);
    }

}
