<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_home")
     */
    public function index(): Response
    {
    
        // Test si connecté :
        $user = $this->getUser();

        // Test si connecté renvoys vers la page Main connect :
        if($user){
            return $this->redirectToRoute('connect_home');
        }




        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }

        /**
     * @Route("/mentionslegal", name="mentions")
     */
    public function mentionslegal(): Response
    {
        return $this->render('main/mentionslegal.html.twig');
    }
}
