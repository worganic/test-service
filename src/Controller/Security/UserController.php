<?php

namespace App\Controller\Security;

use App\Form\EditProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/connect/users", name="users")
     */
    public function index()
    {

        return $this->render('security/users/userProfile.html.twig', [
            'controller_name' => 'users',
        ]);
    }

      /**
     * @Route("/connect/users/profil/edit", name="user_profil_edit")
     */
    public function userEdit(Request $request): Response
    {

        $user = $this->getUser();
        $form =$this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil mise à jour.');
            return $this->redirectToRoute('users');
        }

        return $this->render('security/users/userProfileEdit.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'users',
        ]);
    }

     /**
     * @Route("/connect/users/profil/edit/pass", name="userProfileEditPass")
     */
    public function userEditPass(Request $request, UserPasswordEncoderInterface $userPasswordEncoderInterface): Response
    {
        if($request->isMethod('POST')){
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            // Vérification si les 2 mdp sont identique.
            if($request->request->get('pass') == $request->request->get('pass2')){
                $user->setPassword(
                    $userPasswordEncoderInterface->encodePassword($user, $request->request->get('pass'))
                );
                $em->persist($user);
                $em->flush();

                $this->addFlash('message', 'Mot de passe mise à jour.');
                return $this->redirectToRoute('users');

            } else {
                $this->addFlash('message', 'Erreur de mdp.');
            }

        }
        return $this->render('security/users/userProfileEditPass.html.twig', [
            'controller_name' => 'users',
        ]);
    }
    
}
