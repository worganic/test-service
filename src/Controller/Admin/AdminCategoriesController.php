<?php

namespace App\Controller\Admin;

use Faker;
use App\Form\CategoriesType;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Connect\Categories;
use Doctrine\ORM\EntityRepository;
use App\Repository\CategoriesRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/categories", name="admin_categories_")
 * @package App\Controller
 */
class AdminCategoriesController extends AbstractController
{
    /**
     * 
     * @Route("/", name="home")
     */
    public function index(CategoriesRepository $categoriesRepo)
    {
        $sens = "DESC";
        $search = "";

        return $this->render('admin/categories/index.html.twig', [
            'Categories' => $categoriesRepo->findAll(),
            'sens1' => $sens,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(CategoriesRepository $categoriesRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        
        $repository = $this->getDoctrine()->getRepository(Categories::class);
        $listCategories = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/categories/index.html.twig', [
            'Categories' => $listCategories,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/search", name="recherche")
     */
    public function recherche(CategoriesRepository $categoriesRepo, Request $request)
    {
        $search = $request->request->get('find');
        $sens1 = "DESC";

        $listCategories = $categoriesRepo->findCategories($search);


        return $this->render('admin/categories/index.html.twig', [
            'Categories' => $listCategories,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutCategorie(Request $request)
    {
        $categorie = new Categories;

        $form = $this->createForm(CategoriesType::class, $categorie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('admin_categories_home');
        }

        return $this->render('admin/categories/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/modifier/{id}", name="modif")
     */
    public function ModifCategorie(Categories $categorie, Request $request)
    {
        $form = $this->createForm(CategoriesType::class, $categorie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush();

            return $this->redirectToRoute('admin_categories_home');
        }

        return $this->render('admin/categories/ajout.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */
    public function supprimerCategorie($id, CategoriesRepository $categoriesRepo, Categories $categorie, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $categoriesRepo->find($id);
        $em->remove($donne);
        $em->flush();

        $this->addFlash('error', 'Suppresion de la catégorie "' . $donne->getName() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_categories_home');
    }


    /**
     * @Route("/generer", name="generer")
     */
    public function genererCategorie(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $faker = Faker\Factory::create('fr_FR');
        dump('-----------------------Génération de données Catégories\n');

        for ($donneesNB = 1; $donneesNB < 10; $donneesNB++) {

            $categorie = new Categories();

            $categorie->setCreatedAt($faker->dateTimeBetween($startDate = '-3 years', $endDate = '-1 years', $timezone = null));
            $categorie->setName($faker->text(20));
            // $categories->setSlug($faker->firstName);

            // Verif name pays si exist déjà :

            $repository = $this->getDoctrine()->getRepository(Categories::class);
            $name = $repository->findOneBy(['name' => $categorie->getName()]);
            if (!$name) {
                $manager->persist($categorie);
                dump("categorie name : " . $categorie->getName());
            }
        }

        $this->addFlash('success', '10 nouvelles catégories ont été créé.');
        $manager->flush();

        return $this->redirectToRoute('admin_categories_home');
    }



    /**
     * @Route("/drop", name="drop")
     */
    public function dropCategorie(CategoriesRepository $categoriesRepo)
    {
        //$categoriesRepo->truncateTable();
        $em = $this->getDoctrine()->getManager();

        // Liste les données :
        $donnees = $categoriesRepo->findAll();
        dump($donnees);
        $a = 0;

        foreach ($donnees as $key => $value) {

            $donne = $categoriesRepo->find($value->getId());

            try {
                $em->remove($donne);
                $em->flush();
            } catch (EntityNotFoundException $e) {

                continue;
            }

            // dump("Suppresion de la donnée : ", $value->getId());
        }


        die();
        $this->addFlash('error', 'Toutes les catégories ont été supprimé.');
        return $this->redirectToRoute('admin_categories_home');
    }



    /**
     * @Route("/supp", name="supp")
     */
    public function supprimerPlusieursCategorie(CategoriesRepository $categoriesRepo, Request $request)
    {
        $list = $request->request->get('list');

        $em = $this->getDoctrine()->getManager();

        // Explode de la liste :
        $listExplode = explode("-", $list);
        dump($listExplode);


        foreach ($listExplode as $key) {

            if ($key != "") {

                $donne = $categoriesRepo->find($key);
                //  dump($donne);
                if ($donne != null) {
                    $em->remove($donne);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('admin_categories_home');
    }
}
