<?php
namespace App\Controller\Admin;

use Faker;
use App\Entity\Users;
use App\Entity\Main\Pays;
use App\Form\VoyagesType;
use App\Entity\Connect\Voyages;
use App\Repository\VoyagesRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/voyages", name="admin_voyages_")
 * @package App\Controller
 */
class AdminVoyagesController extends AbstractController
{
     /**
     * 
     * @Route("/", name="home")
     */
    public function index(VoyagesRepository $voyagesRepo)
    {
        $sens = "DESC";
        $search = "";

        return $this->render('admin/voyages/index.html.twig', [
            'Donnees' => $voyagesRepo->findAll(),
            'sens1' => $sens,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(VoyagesRepository $voyagesRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        
        $repository = $this->getDoctrine()->getRepository(Voyages::class);
        $listDonnees = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/voyages/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/search", name="recherche")
     */
    public function recherche(VoyagesRepository $voyagesRepo, Request $request)
    {
        $search = $request->request->get('find');
        $sens1 = "DESC";

        $listDonnees = $voyagesRepo->findDonnees($search);

        return $this->render('admin/voyages/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutVoyages(Request $request)
    {
        $donne = new Voyages;
        $form = $this->createForm(VoyagesType::class, $donne);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($donne);
            $em->flush();

            return $this->redirectToRoute('admin_voyages_home');
        }

        return $this->render('admin/voyages/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => 'Ajouter'
        ]);
    }

    /**
     * @Route("/modifier/{id}", name="modif")
     */
    public function ModifVoyages(Voyages $donne, Request $request)
    {
        $form = $this->createForm(VoyagesType::class, $donne);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($donne);
            $em->flush();

            return $this->redirectToRoute('admin_voyages_home');
        }

        $titleSection = "Modifier le voyage";
        return $this->render('admin/voyages/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => $titleSection
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */
    public function supprimerVoyages($id, VoyagesRepository $voyagesRepo, Voyages $donne, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $voyagesRepo->find($id);
        $em->remove($donne);
        $em->flush();

        $this->addFlash('error', 'Suppresion du voyage "' . $donne->getTitre() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_voyages_home');
    }

    /**
     * @Route("/supprimer/{id}/user/{idUser}", name="supprimerVoyageByUser")
     */
    public function supprimerVoyagesUser($id, $idUser, VoyagesRepository $voyagesRepo, Voyages $donne, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $voyagesRepo->find($id);
    //    $em->remove($donne);
    //    $em->flush();

        $urlParameters[ "id" ] = $idUser ;
        $this->addFlash('error', 'Suppresion du voyage "' . $donne->getTitre() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_users_modif', $urlParameters);
    }




    /**
     * @Route("/generer", name="generer")
     */
    public function genererVoyages(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $faker = Faker\Factory::create('fr_FR');
        dump('-----------------------Génération de données Voyages\n');

        // Récupère la liste des pays :
        $repoPays = $this->getDoctrine()->getRepository(Pays::class);
        $pays = $repoPays->findAll();
        $repoUsers = $this->getDoctrine()->getRepository(Users::class);
        $users = $repoUsers->findAll();
        $countUSer = count($users) - 1;
        dump($users);

        for ($donneesNB = 1; $donneesNB < 10; $donneesNB++) {

            $voyage = new Voyages();

            $fak = $faker->numberBetween(0,$countUSer);
            $user = $users[$fak];

            $voyage->setUsers($user);
            $voyage->addPay($pays[$faker->numberBetween(0,count($pays))]);
            $voyage->setTitre("Voyage " . $faker->realText(10));
            $voyage->setDateTimeDebut($faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now', 
            $timezone = null));
            $voyage->setDateTimeFin($faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now', 
            $timezone = null));
            $voyage->setParticipantNbAdultes($faker->numberBetween(1,2));
            $voyage->setParticipantNbEnfants($faker->numberBetween(0,4));
         
            // Verif name Voyages si exist déjà :
            $repoVoyages = $this->getDoctrine()->getRepository(Voyages::class);
            $titre = $repoVoyages->findOneBy(['titre' => $voyage->getTitre()]);
            if (!$titre) {
                $manager->persist($voyage);
                dump("voyages ajouté : " . $voyage->getTitre());
            } else {
                $donneesNB = $donneesNB - 1;
            }
        }

        $this->addFlash('success', '10 nouveaux voyages ont été créé.');
        $manager->flush();

        return $this->redirectToRoute('admin_voyages_home');
    }



    /**
     * @Route("/drop", name="drop")
     */
    public function dropVoyages(VoyagesRepository $voyagesRepo)
    {
        //$categoriesRepo->truncateTable();
        $em = $this->getDoctrine()->getManager();

        // Liste les données :
        $donnees = $voyagesRepo->findAll();
        dump($donnees);
        $a = 0;

        foreach ($donnees as $key => $value) {

            $donne = $voyagesRepo->find($value->getId());
            try {
                $em->remove($donne);
                $em->flush();
            } catch (EntityNotFoundException $e) {
                continue;
            }
        }

        $this->addFlash('error', 'Tous les voyages ont été supprimé.');
        return $this->redirectToRoute('admin_voyages_home');
    }



    /**
     * @Route("/supp", name="supp")
     */
    public function supprimerPlusieursVoyages(VoyagesRepository $voyagesRepo, Request $request)
    {
        $list = $request->request->get('list');

        $em = $this->getDoctrine()->getManager();

        // Explode de la liste :
        $listExplode = explode("-", $list);
        dump($listExplode);


        foreach ($listExplode as $key) {

            if ($key != "") {

                $donne = $voyagesRepo->find($key);
                //  dump($donne);
                if ($donne != null) {
                    $em->remove($donne);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('admin_voyages_home');
    }




    /**
     * @Route("/include", name="include")
     */
    public function testVoyages(VoyagesRepository $voyagesRepo, $user)
    {

        $listDonnees = $voyagesRepo->findByUser($user);


        $sens = "DESC";
        $search = "";

        return $this->render('admin/voyages/include.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens,
            'find' => $search
        ]);
    }
}