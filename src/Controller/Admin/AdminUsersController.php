<?php

namespace App\Controller\Admin;

use Faker;
use App\Entity\Users;
use App\Form\EditProfileType;
use App\Form\UsersType;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/admin/users", name="admin_users_")
 * @package App\Controller
 */
class AdminUsersController extends AbstractController
{
    /**
     * 
     * @Route("/", name="home")
     */
    public function index(UsersRepository $usersRepo)
    {
        $sens = "DESC";
        $search = "";

        return $this->render('admin/users/index.html.twig', [
            'Donnees' => $usersRepo->findAll(),
            'sens1' => $sens,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(UsersRepository $usersRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        
        $repository = $this->getDoctrine()->getRepository(Users::class);
        $listDonnees = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/users/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * 
     * @Route("/search", name="recherche")
     */
    public function recherche(UsersRepository $usersRepo, Request $request)
    {
        $search = $request->request->get('find');
        $sens1 = "DESC";

        $listDonnees = $usersRepo->findDonnees($search);

        return $this->render('admin/users/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutUser(Request $request)
    {
        $user = new Users;
        $form = $this->createForm(UsersType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_home');
        }

        return $this->render('admin/users/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => 'Ajouter'
        ]);
    }

    /**
     * @Route("/modifier/{id}", name="modif")
     */
    public function ModifUser(Users $user, Request $request)
    {
        $form = $this->createForm(UsersType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_home');
        }
        $titleSection = "Modification de l'utilisateur " . $user->getName() . " - " .  $user->getFirstname() . "." ;

        return $this->render('admin/users/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => $titleSection,
            'user' => $user
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */
    public function supprimerUsers($id, UsersRepository $usersRepo, Users $user, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $usersRepo->find($id);
        $em->remove($donne);
        $em->flush();

        $this->addFlash('error', 'Suppresion du user "' . $donne->getName() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_users_home');
    }


    /**
     * @Route("/generer", name="generer")
     */
    public function genererUsers(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $faker = Faker\Factory::create('fr_FR');
        dump('-----------------------Génération de données Users\n');

        for ($donneesNB = 1; $donneesNB < 10; $donneesNB++) {

            $user = new Users();







            

            // Verif name user si exist déjà :
            $repository = $this->getDoctrine()->getRepository(Users::class);
            $name = $repository->findOneBy(['name' => $user->getName()]);
            if (!$name) {
                $manager->persist($user);
                dump("users name : " . $user->getName());
            }
        }

        $this->addFlash('success', '10 nouveaux users ont été créé.');
        $manager->flush();

        return $this->redirectToRoute('admin_users_home');
    }



    /**
     * @Route("/drop", name="drop")
     */
    public function dropUsers(UsersRepository $usersRepo)
    {
        //$categoriesRepo->truncateTable();
        $em = $this->getDoctrine()->getManager();

        // Liste les données :
        $donnees = $usersRepo->findAll();
        dump($donnees);
        $a = 0;

        foreach ($donnees as $key => $value) {

            $donne = $usersRepo->find($value->getId());

            try {
                $em->remove($donne);
                $em->flush();
            } catch (EntityNotFoundException $e) {
                continue;
            }

            // dump("Suppresion de la donnée : ", $value->getId());
        }


        die();
        $this->addFlash('error', 'Toutes les catégories ont été supprimé.');
        return $this->redirectToRoute('admin_users_home');
    }



    /**
     * @Route("/supp", name="supp")
     */
    public function supprimerPlusieursUsers(UsersRepository $usersRepo, Request $request)
    {
        $list = $request->request->get('list');

        $em = $this->getDoctrine()->getManager();

        // Explode de la liste :
        $listExplode = explode("-", $list);
        dump($listExplode);


        foreach ($listExplode as $key) {

            if ($key != "") {

                $donne = $usersRepo->find($key);
                //  dump($donne);
                if ($donne != null) {
                    $em->remove($donne);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('admin_users_home');
    }
}