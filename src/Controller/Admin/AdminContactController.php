<?php

namespace App\Controller\Admin;

use App\Entity\Main\Contact;

use App\Repository\ContactRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/admin/contact", name="admin_contact_")
 * @package App\Controller
 */
class AdminContactController extends AbstractController
{
    /**
     * 
     * @Route("/", name="home")
     */
    public function index(ContactRepository $contactRepo)
    {
        return $this->render('admin/contact/index.html.twig', [
            'Contacts' => $contactRepo->findAll()
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutUsers(Request $request)
    {
        $contacts = new Contact;
/*
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_home');
        }

        return $this->render('admin/users/ajout.html.twig', [
            'form' => $form->createView()
        ]);
        */
    }

     /**
     * @Route("/modif", name="modif")
     */
    public function modifUsers(Request $request)
    {
        $contacts = new Contact;
/*
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users_home');
        }

        return $this->render('admin/users/ajout.html.twig', [
            'form' => $form->createView()
        ]);
        */
    }
}