<?php

namespace App\Controller\Admin;

use Faker;
use App\Entity\Users;
use App\Form\MonSacType;
use App\Entity\Connect\MonSac;
use App\Entity\Connect\Categories;
use App\Repository\MonSacRepository;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/admin/monsac", name="admin_monsac_")
 * @package App\Controller
 */
class AdminMonsacController extends AbstractController
{
     public $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

     /**
     * 
     * @Route("/", name="home")
     */
    public function index(MonsacRepository $monsacRepo)
    {
        $sens = "DESC";
        $search = "";
        $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

        return $this->render('admin/monsac/index.html.twig', [
            'Donnees' => $monsacRepo->findAll(),
            'sens1' => $sens,
            'find' => $search,
            'listeEtat' => $listeEtat
        ]);
    }

    /**
     * 
     * @Route("/tri/{col}/{sens}", name="tri")
     */
    public function tri(MonsacRepository $monsacRepo, $col, $sens)
    {
        $sens1 = "DESC";
        $search = "";
        $listeEtat = ['Pourquoi pas', 'A acheter', 'Acheté'];

        $repository = $this->getDoctrine()->getRepository(Monsac::class);
        $listDonnees = $repository->findBy(array(), array($col => $sens),     null,     null);

        if ($sens == $sens1) {
            $sens1 = "ASC";
        }

        return $this->render('admin/monsac/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search,
            'listeEtat' => $listeEtat
        ]);
    }

    /**
     * 
     * @Route("/search", name="recherche")
     */
    public function recherche(MonsacRepository $monsacRepo, Request $request)
    {
        $search = $request->request->get('find');
        $sens1 = "DESC";

        $listDonnees = $monsacRepo->findDonnees($search);

        return $this->render('admin/monsac/index.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens1,
            'find' => $search
        ]);
    }

    /**
     * @Route("/ajout", name="ajout")
     */
    public function ajoutMonsac(Request $request)
    {
        $donne = new Monsac;
      
        $form = $this->createForm(MonsacType::class, $donne);
        $form->handleRequest($request);
      
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($donne);
            $em->flush();

            return $this->redirectToRoute('admin_monsac_home');
        }

        return $this->render('admin/monsac/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => 'Ajouter'
        ]);
    }

    /**
     * @Route("/modifier/{id}", name="modif")
     */
    public function ModifMonsac(Monsac $donne, Request $request)
    {
        $form = $this->createForm(MonsacType::class, $donne);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($donne);
            $em->flush();

            return $this->redirectToRoute('admin_monsac_home');
        }

        $titleSection = "Modifier le sac";
        return $this->render('admin/monsac/ajout.html.twig', [
            'form' => $form->createView(),
            'titleSection' => $titleSection
        ]);
    }

    /**
     * @Route("/supprimer/{id}", name="supprimer")
     */
    public function supprimerMonsac($id, MonsacRepository $monsacRepo, Monsac $donne, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $monsacRepo->find($id);
        $em->remove($donne);
        $em->flush();

        $this->addFlash('error', 'Suppression du sac "' . $donne->getTitle() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_monsac_home');
    }

    /**
     * @Route("/supprimer/{id}/user/{idUser}", name="supprimerSacByUser")
     */
    public function supprimerMonsacUser($id, $idUser, MonsacRepository $monsacRepo, Monsac $donne, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $donne = $monsacRepo->find($id);
    //    $em->remove($donne);
    //    $em->flush();

        $urlParameters[ "id" ] = $idUser ;
        $this->addFlash('error', 'Suppression du sac "' . $donne->getTitle() . '" (' . $id . ').');
        return $this->redirectToRoute('admin_users_modif', $urlParameters);
    }




    /**
     * @Route("/generer", name="generer")
     */
    public function genererMonsac(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $faker = Faker\Factory::create('fr_FR');
        dump('-----------------------Génération de données Monsac\n');

        $repoUsers = $this->getDoctrine()->getRepository(Users::class);
        $users = $repoUsers->findAll();
        $countUSer = count($users) - 1;

        $repoCategories = $this->getDoctrine()->getRepository(Categories::class);
        $categories = $repoCategories->findAll();
        $countCategorie = count($categories) - 1;

        $liste = array(
            [
                'Title' => 'Sac à dos', 
                'Content' => 'SALOMON Trailblazer 10 Sac A Dos Randonnée Mixte', 
                'Active' => true,
                'Url' => 'https://www.amazon.fr/MOUNTAINTOP-Randonn%C3%A9e-Trekking-Camping-Alpinisme/dp/B07CL3TCFM/ref=sr_1_2_sspa?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=SALOMON+Trailblazer+10+Sac+A+Dos+Randonn%C3%A9e+Mixte&qid=1623837694&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUEzOUUzTlpKREFDN0s1JmVuY3J5cHRlZElkPUEwMjgzMzczRk1VNVRHQlNNUUFUJmVuY3J5cHRlZEFkSWQ9QTA0NzEyMzczVU1NTk1BWUxFTEw4JndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==',
                'Budgets_prevus' => 80,
                'Budgets_paye' => 59,

                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
            [
                'Title' => 'Lumière', 
                'Content' => 'CHENAN Lampe Frontale，Lampe Frontale Puissante,Lampe Torche 8 Modes USB Rechargeable 18000 Lumens avec 8 LEDs, IPX4 Étanche Lampe Frontale pour Le Camping, la Pêche, Le Jogging et la Randonnée', 
                'Active' => false, 
                'Url' => 'https://www.amazon.fr/CHENAN-Frontale%EF%BC%8CLampe-Puissante-Rechargeable-Randonn%C3%A9e/dp/B08CVSWY7Q/ref=sr_1_2_sspa?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=CHENAN+Lampe+Frontale%EF%BC%8CLampe+Frontale+Puissante%2CLampe+Torche+8+Mode&qid=1623837748&sr=8-2-spons&psc=1&spLa=ZW5jcnlwdGVkUXVhbGlmaWVyPUFCUFZZVEtWQUNSMlEmZW5jcnlwdGVkSWQ9QTAxNDEzMjgzQ0pMVkFNTkNNT0c2JmVuY3J5cHRlZEFkSWQ9QTA5NzU0NjQyR1A5QUpBNDE3QlA1JndpZGdldE5hbWU9c3BfYXRmJmFjdGlvbj1jbGlja1JlZGlyZWN0JmRvTm90TG9nQ2xpY2s9dHJ1ZQ==',
                'Budgets_prevus' => 25,
                'Budgets_paye' => 19,

                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
            [
                'Title' => 'Gourde 1L', 
                'Content' => 'Super Sparrow Gourde Isotherme - Bouteille Isotherme - 350ml, 500ml, 620ml, 750ml, 1L - Gourde Inox Bouche Standard - Gourde Sport Étanche - Gourde Enfant sans BPA, pour Sport, Gym, Voyage', 
                'Active' => true, 
                'Url' => 'https://www.amazon.fr/Super-Sparrow-Bouteille-inoxydable-standard/dp/B07K9YKHPR/ref=sr_1_1?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Super+Sparrow+Gourde+Isotherme+-+Bouteille+Isotherme+-+350ml%2C+500ml%2C&qid=1623837840&sr=8-1',
                'Budgets_prevus' => 20,
                'Budgets_paye' => 26,
                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
            [
                'Title' => 'Chaussure', 
                'Content' => 'Easondea Chaussures de Randonnée pour Hommes Femmes Bottes de Randonnée Unisexe Chaussures de Marche en Plein Air Bottes Antidérapantes Trekking et Les Promenades', 
                'Active' => true, 
                'Url' => 'https://www.amazon.fr/NEOKER-Chaussures-Randonn%C3%A9e-Trekking-Promenades/dp/B075SZGWKC/ref=sr_1_7?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Easondea+Chaussures+de+Randonn%C3%A9e+pour+Hommes+Femmes+Bottes+de+Randonn%C3%A9e+Unise&qid=1623837872&sr=8-7',
                'Budgets_prevus' => 50,
                'Budgets_paye' => 41,
                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
            [
                'Title' => 'Camera', 
                'Content' => 'GoPro HERO7 Caméra Numérique Embarquée Étanche avec Écran Tactile, Vidéo HD 4K, Photos 12 MP, Diffusion en Direct et Stabilisation Intégrée', 
                'Active' => false, 
                //'Url' => 'https://www.amazon.fr/GoPro-HERO7-Black-num%C3%A9rique-stabilisation/dp/B07GSVDFTQ/ref=sr_1_2?__mk_fr_FR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=GoPro+HERO7+Cam%C3%A9ra+Num%C3%A9rique+Embarqu%C3%A9e+%C3%89tanche+avec+%C3%89cran+Tactile%2C+Vid%C3%A9o+HD+4K%2C+Photos+12+MP&qid=1623837917&sr=8-2',
                'Url' => 'test',
                'Budgets_prevus' => 300,
                'Budgets_paye' => 280,
                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
            /*
            [
                'Title' => '', 
                'Content' => '', 
                'Active' => false, 
                'Url' => '',
                'Budgets_prevus' => '',
                'Budgets_paye' => '',
                //'Categorie' => 'Sac à dos', 
                //'Users' => 'Sac à dos', 
            ],
*/
        );

dump($liste);           


    $a = 0;
    foreach($liste AS $key => $value){


            $donne = new Monsac();

            $fak = $faker->numberBetween(0,$countUSer);
            $user = $users[$fak];

            $fak = $faker->numberBetween(0,$countCategorie);
            $categorie = $categories[$fak];

        //    dump("Monsac categorie : " . $categorie);
        //    dump("Monsac user : " . $user);
           
            $donne->setUsers($user);
            $donne->setCategories($categorie);
            $donne->setTitle($value['Title']);
            $donne->setContent($value['Content']);
            $donne->setUrl($value['Url']);//
            $donne->setDateAchat(
                $faker->dateTimeBetween(
                    $startDate = '-5 years', 
                    $endDate = 'now', 
                    $timezone = null
                )
            );
            $donne->setActive(1);
            $donne->setActive($value['Active']);
            $donne->setEtat($faker->numberBetween(0,2));
            $donne->setBudgetsPrevus($value['Budgets_prevus']);
            $donne->setBudgetsPaye($value['Budgets_paye']);

            // Verif name Monsac si exist déjà :
            $repository = $this->getDoctrine()->getRepository(Monsac::class);
            $name = $repository->findOneBy(['title' => $donne->getTitle()]);
            if (!$name) {
                $manager->persist($donne);
                dump("Monsac name : " . $donne->getTitle());
                dump( $donne);
                $a++;
            }
      
        }


        $this->addFlash('success', '10 nouveaux Monsac ont été créé.');
        $manager->flush();

        return $this->redirectToRoute('admin_monsac_home');
    }



    /**
     * @Route("/drop", name="drop")
     */
    public function dropMonsac(MonsacRepository $monsacRepo)
    {
        //$categoriesRepo->truncateTable();
        $em = $this->getDoctrine()->getManager();

        // Liste les données :
        $donnees = $monsacRepo->findAll();
        dump($donnees);
        $a = 0;

        foreach ($donnees as $key => $value) {

            $donne = $monsacRepo->find($value->getId());

            try {
                $em->remove($donne);
                $em->flush();
            } catch (EntityNotFoundException $e) {
                continue;
            }
            dump("Suppresion de la donnée : ", $value->getId());
        }

        $this->addFlash('error', 'Tous les Monsac ont été supprimé.');
        return $this->redirectToRoute('admin_monsac_home');
    }



    /**
     * @Route("/supp", name="supp")
     */
    public function supprimerPlusieursMonsac(MonsacRepository $monsacRepo, Request $request)
    {
        $list = $request->request->get('list');

        $em = $this->getDoctrine()->getManager();

        // Explode de la liste :
        $listExplode = explode("-", $list);
        dump($listExplode);


        foreach ($listExplode as $key) {

            if ($key != "") {

                $donne = $monsacRepo->find($key);
                //  dump($donne);
                if ($donne != null) {
                    $em->remove($donne);
                    $em->flush();
                }
            }
        }

        return $this->redirectToRoute('admin_monsac_home');
    }

    /**
     * @Route("/include", name="include")
     */
    public function testMonsac(MonsacRepository $monsacRepo, $user)
    {

        $listDonnees = $monsacRepo->findByUser($user);


        $sens = "DESC";
        $search = "";

        return $this->render('admin/monsac/include.html.twig', [
            'Donnees' => $listDonnees,
            'sens1' => $sens,
            'find' => $search
        ]);
    }
}