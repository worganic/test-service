<?php
/* Création : 2021-06-08
 * Dernière MAJ : 2021-06-08
 * Créateur : Johann Loreau
 * Lieu : Marseille
*/
namespace App\DataFixtures;

use Faker;
use App\Entity\Connect\Categories;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CategoriesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        echo '-----------------------CategoriesFixtures\n';
        $faker = Faker\Factory::create('fr_FR');

        $categories = [
            1 => ['name' => 'Transport'],
            2 => ['name' => 'Utils'],
            3 => ['name' => 'Allemagne'],
            4 => ['name' => 'Suisse'],
            5 => ['name' => 'Norvège'],
            6 => ['name' => 'Italie'],
        ];
        /*

        for($contactNB = 1; $contactNB < 10; $contactNB++){

            $contact = new Contact();
            
            $user = $this->getReference('user_' . $faker->numberBetween(1,9));

            $contact->setIp("0.0.0.0.0");
            $contact->setDateTime($faker->dateTimeBetween($startDate = '-3 years', $endDate = '-1 years', $timezone = null));
            $contact->setMail($faker->email);
            $contact->setNom($faker->firstName);
            $contact->setTelephone("0601020304");//$faker->phoneNumber
            $contact->setTexte($faker->text(100));
            $contact->setValid(1);
            $contact->setUser($user);

            $manager->persist($contact);
        }

        $manager->flush();
        /*
        
        */

    }


}