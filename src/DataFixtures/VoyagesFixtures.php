<?php
/* Création : 2021-06-01
 * Dernière MAJ : 2021-06-01
 * Créateur : Johann Loreau
 * Lieu : Marseille
*/
namespace App\DataFixtures;

use Faker;
use App\Entity\Connect\Voyages;
use App\DataFixtures\UsersFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class VoyagesFixtures extends Fixture implements DependentFixtureInterface
{
    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        echo '-----------------------VoyagesFixtures\n';
        $faker = Faker\Factory::create('fr_FR');
        
        for($nbVoyages = 1; $nbVoyages < 50; $nbVoyages++){
      
            $pay = $this->getReference('pay_' . $faker->numberBetween(0,145));
            $user = $this->getReference('user_' . $faker->numberBetween(1,9));

            $voyage = new Voyages();
            $voyage->setUser($user);
            $voyage->addPay($pay);
            $voyage->setTitre($faker->realText(25));
            $voyage->setDateTimeDebut($faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now', 
            $timezone = null));
            $voyage->setDateTimeFin($faker->dateTimeBetween($startDate = '-10 years', $endDate = 'now', 
            $timezone = null));
            $voyage->setParticipantNbAdultes($faker->numberBetween(1,2));
            $voyage->setParticipantNbEnfants($faker->numberBetween(0,4));
         
            // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
            $this->addReference('voyage'. $nbVoyages, $voyage);

            $manager->persist($voyage);
        }


         $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UsersFixtures::class
        ];
    }

}