<?php
/* Création : 2021-06-08
 * Dernière MAJ : 2021-06-08
 * Créateur : Johann Loreau
 * Lieu : Marseille
*/
namespace App\DataFixtures;

use App\Entity\Main\Logs;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class LogsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      
        echo '-----------------------LogsFixtures\n';
        $faker = Faker\Factory::create('fr_FR');
        
        for($donneesNB = 1; $donneesNB < 10; $donneesNB++){

            $log = new Logs();
            
            $log->setDateTime($faker->dateTimeBetween($startDate = '-3 years', $endDate = '-1 years', $timezone = null));
            $log->setType($faker->text);
            $log->setDivers($faker->text);
            $log->setText($faker->text);
            $log->setEtat($faker->text);

            $manager->persist($log);
        }

    }

}