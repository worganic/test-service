<?php
/* Création : 2021-06-01
 * Dernière MAJ : 2021-06-01
 * Créateur : Johann Loreau
 * Lieu : Marseille
*/
namespace App\DataFixtures;

use Faker;
use App\Entity\Users;
use App\DataFixtures\PaysFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsersFixtures extends Fixture implements DependentFixtureInterface
{
    private $encoder;


    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        echo '-----------------------UsersFixtures\n';
        $faker = Faker\Factory::create('fr_FR');
        
        for($nbUsers = 1; $nbUsers < 10; $nbUsers++){

            $pay = $this->getReference('pay_' . $faker->numberBetween(0,145));

            $user = new Users();
            
            if($nbUsers == 1){
                $user->setEmail("test@test.com");
                $user->setRoles(['ROLE_ADMIN']);
                $user->setName("testNom");
                $user->setFirstname("TestFirsname");
                $user->setPassword($this->encoder->encodePassword($user, 'azerty'));
                $user->setPseudo("testPseudo");
            }else{
                $user->setEmail($faker->email);
                $user->setRoles(['ROLE_USER']);
                $user->setName($faker->lastName);
                $user->setFirstname($faker->firstName);
                $user->setPseudo($faker->firstName);
                $user->setPassword($this->encoder->encodePassword($user, 'azerty'));
            }
            
            $user->setDateNaissance($faker->dateTimeBetween($startDate = '-90 years', $endDate = '-18 years', 
            $timezone = null));
            $user->setSexe($faker->numberBetween(0,1));
            $user->setIsVerified($faker->numberBetween(0,1));
            $user->setPays($pay);

            // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
            $this->addReference('user_'. $nbUsers, $user);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            PaysFixtures::class,
        );
    }

}