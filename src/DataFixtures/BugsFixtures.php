<?php
/* Création : 2021-06-08
 * Dernière MAJ : 2021-06-08
 * Créateur : Johann Loreau
 * Lieu : Marseille
*/
namespace App\DataFixtures;

use App\Entity\Main\Bugs;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class BugsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      
        echo '-----------------------BugsFixtures\n';
        $faker = Faker\Factory::create('fr_FR');
        
        for($contactNB = 1; $contactNB < 10; $contactNB++){

            $bug = new Bugs();
            
            $bug->setDateTime($faker->dateTimeBetween($startDate = '-3 years', $endDate = '-1 years', $timezone = null));
            $bug->setCredicite(1);
            $bug->setUrl($faker->firstName);
            $bug->setCommentaires($faker->text(100));
            $bug->setEtat(1);
            $bug->setDateTimeRetour($faker->dateTimeBetween($startDate = '-3 years', $endDate = '-1 years', $timezone = null));
            $bug->setRetour($faker->text(20));
            //$bug->ssetUser();

            $manager->persist($bug);
        }

        $manager->flush();

    }


}