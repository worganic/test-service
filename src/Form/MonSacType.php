<?php
namespace App\Form;

use App\Entity\Users;
use App\Entity\Connect\MonSac;
use App\Entity\Connect\Categories;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ContainerPcMhPl9\getFosCkEditor_Form_TypeService;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MonSacType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre :',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('Slug', TextType::class, [
                'label' => 'Slug :',
                'disabled' => true,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('Content', CKEditorType::class) // Ce champ sera remplacé par un éditeur WYSIWYG
            ->add('active', CheckboxType::class, [
                'label'    => 'Actif :',
                'required' => false,
            ])
            ->add('url', TextType::class, [
                'label'    => 'Url :',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('Budgets_prevus', IntegerType::class, [
                'label'    => 'Budget prévus :',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1',
                    'style' => 'width:80px;'
                )
            ])

            ->add('Budgets_paye', IntegerType::class, [
                'label'    => 'Budget payé :',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1',
                    'style' => 'width:80px;'
                )
            ])
            ->add('Etat', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                ),
                'choices'  => [
                    'Pourquoi pas' => '0',
                    'A acheter' => '1',
                    'Acheté' => '2',
                ],
            ])
            /*
            ->addModelTransformer(new CallbackTransformer(
                function ($etatArray) {
                        // transform the array to a string
                        return count($etatArray)? $etatArray[0]: null;
                        //return null;
                },
                function ($etatString) {
                        // transform the string back to an array
                        return [$etatString];
                }
            ))
            */
            ->add('users', EntityType::class, [
                'label' => 'Utilisateur :',
                'class' => Users::class,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('categories', EntityType::class,  [
                'label' => 'Categories :',
                'class' => Categories::class,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('date_achat', DateType::class, [
                'label' => 'Date d\'achat :',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])


            ->add('save', SubmitType::class, [
                'attr' => array(
                    'class' => 'btn shadow-1 rounded-1 small primary uppercase'
                )
            ])
        ;

      
           
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MonSac::class,
        ]);
    }
}
