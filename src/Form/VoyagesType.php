<?php
namespace App\Form;

use App\Entity\Users;
use App\Entity\Main\Pays;
use App\Entity\Connect\Voyages;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class VoyagesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', TextType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            
            ->add('created_at', DateType::class, [
                'label' => 'Créé le :',
                'data' => new \DateTime("now"),
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('dateTime_debut', DateType::class, [
                'label' => 'Début du voyage :',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('dateTime_fin', DateType::class, [
                'label' => 'Fin du voyage :',
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('participantNbAdultes', NumberType::class, [
                'label' => 'Participants adultes :',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('participantNbEnfants', NumberType::class,
             
             [
                'label' => 'Participants enfants :',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('users', EntityType::class, [
                'class' => Users::class,
                'label' => 'Utilisateur :',
                // 'mapped' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('pays', EntityType::class, [
                'class' => Pays::class,
                'multiple' => true, 
                'label' => 'Pays :',
             //   'mapped' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
           


            ->add('save', SubmitType::class, [
                'attr' => array(
                    'class' => 'btn shadow-1 rounded-1 small primary uppercase'
                )
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Voyages::class,
        ]);
    }
}
