<?php

namespace App\Form;

use App\Entity\Users;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('firstname', TextType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('pseudo', TextType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('email', EmailType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                ),
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci d\'entrer un e-mail',
                    ]),
                ],
                'required' => true,
            ])
            ->add('date_naissance', DateTimeType::class, [
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )
            ])
            ->add('sexe', CheckboxType::class)
        //    ->add('roles', TextType::class)
        
            ->add('Roles', ChoiceType::class, [
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'attr' => array(
                    'class' => 'form-control rounded-1'
                ),
                'choices'  => [
                'User' => 'ROLE_USER',
                'Partner' => 'ROLE_PARTNER',
                'Admin' => 'ROLE_ADMIN',
                
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => ['label' => 'Password',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )],
                'second_options' => ['label' => 'Confirm Password',
                'attr' => array(
                    'class' => 'form-control rounded-1'
                )],
                
            ])

        //    ->add('isVerified', TextType::class)
        //    ->add('date_naissance', TextType::class)
        //    ->add('sexe', TextType::class)        
    //    voyages
    //    pays
            ->add('save', SubmitType::class, [
                'attr' => array(
                    'class' => 'btn shadow-1 rounded-1 small primary uppercase'
                )
            ])
        ;

         // Data transformer
         $builder->get('Roles')
         ->addModelTransformer(new CallbackTransformer(
             function ($rolesArray) {
                  // transform the array to a string
                  return count($rolesArray)? $rolesArray[0]: null;
             },
             function ($rolesString) {
                  // transform the string back to an array
                  return [$rolesString];
             }
     ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
