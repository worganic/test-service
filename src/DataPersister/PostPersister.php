<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\MonSac;

class PostPersister implements DataPersisterInterface
{
    protected $em;

    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }


    public function supports($data): bool
    {
        return $data instanceof MonSac;
    }

    public function persist($data)
    {
        // 1. Mettre une date de création sur le sac
        $data->setCreatedAt(new \DateTime());

        // 2. doctrine persiste
        $this->em->persist($data);
        $this->em->flush();
    }

    public function remove($data)
    {
        // 1. Mettre une date de création sur le sac



    }

}